---
title: "About"
image: "profile.jpg"
weight: 8
---

Professional Engineer with expertise in data science, big data, advanced analytics and leading teams. Adept at tackling unique and complex problems that can be addressed using data science, machine learning and data visualization. Accomplished in facilitating broad organizational change to adopt a data centric culture and become more analytically focused. Driving engagement with diverse business units to build skills, capabilities and drive results. An expert presenter and motivator with experience presenting internally at the executive level or more broadly externally. 

### Skills

* Data architecture and cloud infrastructure  
* machine learning models
* cleaning, preparing, and verifying data
* selecting and validating data models
* developing data driven solutions
* statistical modeling
* creating and presenting executive level presentations on business solutions
* using programming skills, including SQL and R, Python, or SAS
* Develop of training programs to build the capacity and skill set of team members. Recent work is centred around data science, machine learning, modelling and optimization, as well as building institutional capacity.
